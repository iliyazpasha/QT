create or replace procedure mapping_BS 
is
begin
  execute immediate 'insert into BS (SELECT CO_CODE,CO_NAME,YearEnd,GrossBlock+CapitalWorkinProgress+ProducingProperties as GBV,NetBlock+LeaseAdjustments+CapitalWorkinProgress+ProducingProperties as NBV,Investments as investments,Inventories as investments_currentsassets_inv,SundryDebtors as investments_currentsassets_rec,CashandBankBalance+BalanceatBankandCallMoney as investments_cash_bank,Loans+Advances+LoansandAdvances as  investments_loans_adv,TotalCurrentAssets as investments_total,CurrentLiabilities as investments_currentLiabilities,PolicyHoldersFund+Provisions as  investments_c_others,TotalCurrentLiabilities as investments_c_total,NetCurrentAssets as net_working_capital,NetDeferredTax as deferredTax,miscellaneousexpensnotwriten+OtherAssets as other_Assets,TotalAssets as totalAssets,ShareCapital as equity,ReservesTotal+EquityApplicationMoney+EquityShareWarrants+MinorityInterest as reserves,TotalShareholdersFunds as net_worth,SecuredLoans as securedLoans,UnsecuredLoans as unsecuredLoans,TotalDebtLoanFunds as long_term_debts,OtherLiabilities as otherLiabilities,TotalLiabilities as total_liabilities  from  temp1)';
end;
------------------------------------------------------------------------------------------------------------------
create or replace procedure mapping_PL()
is
begin
  execute immediate 'insert into PL (select CO_CODE,CO_NAME,YearEnd,NetSales as sales,StockAdjustments+RawMaterials+PowerampFuelCost+OtherManufacturingExpenses as Mfg,NetSales-(StockAdjustments+RawMaterials+PowerampFuelCost+OtherManufacturingExpenses) as GP,EmployeeCost+SellingandAdministrationExp+MiscellaneousExpenses+lesspreoperativeexpenses as OH,(NetSales-(StockAdjustments+RawMaterials+PowerampFuelCost+OtherManufacturingExpenses)-(EmployeeCost+SellingandAdministrationExp+MiscellaneousExpenses+lesspreoperativeexpenses)) as EBITDA,Depreciation as DEP,((NetSales-(StockAdjustments+RawMaterials+PowerampFuelCost+OtherManufacturingExpenses)-(EmployeeCost+SellingandAdministrationExp+MiscellaneousExpenses+lesspreoperativeexpenses))-Depreciation) as EBIT,Interest as interest,(((NetSales-(StockAdjustments+RawMaterials+PowerampFuelCost+OtherManufacturingExpenses)-(EmployeeCost+SellingandAdministrationExp+MiscellaneousExpenses+lesspreoperativeexpenses))-Depreciation)-Interest) as EBT,OtherIncome as otherincome,((((NetSales-(StockAdjustments+RawMaterials+PowerampFuelCost+OtherManufacturingExpenses)-(EmployeeCost+SellingandAdministrationExp+MiscellaneousExpenses+lesspreoperativeexpenses))-Depreciation)-Interest)-OtherIncome) as pbt,Tax+FringeBenefit+DeferredTax as Tax,(((((NetSales-(StockAdjustments+RawMaterials+PowerampFuelCost+OtherManufacturingExpenses)-(EmployeeCost+SellingandAdministrationExp+MiscellaneousExpenses+lesspreoperativeexpenses))-Depreciation)-Interest)-OtherIncome)-(Tax+FringeBenefit+DeferredTax)) as pat,ExtraordinaryItems as Exord_items,((((((NetSales-(StockAdjustments+RawMaterials+PowerampFuelCost+OtherManufacturingExpenses)-(EmployeeCost+SellingandAdministrationExp+MiscellaneousExpenses+lesspreoperativeexpenses))-Depreciation)-Interest)-OtherIncome)-(Tax+FringeBenefit+DeferredTax))-ExtraordinaryItems) as ajust from temp2)';
end;
----------------------------------------------------------------------------------
create or replace procedure mapping_QY()
is
begin
  execute immediate 'insert into QY (SELECT CO_CODE,CO_NAME,Year,TotalIncome as Net_sales,PBIDT as EBITDA,Depreciation as DEP, PBIDT-Depreciation as EBIT,Interest as interest,Tax+DeferredTax+FringeBenefitTax as Tax,EPSafterExceptionalExtraordinaryitemsBasic as EPS,MarketCapitalisation as Market_cap from temp3)';
end;
--------------------------------------------------------------------------------------------
create or replace procedure mapping_CF 
is
begin
  execute immediate 'insert into CF (SELECT CO_CODE,CO_NAME,YearEnd,NetCashfromOperating as operating,NetCashUsedinInvesting as investing,NetCashUsedinFinancing as financing,NetIncDecinCashandCash as net_cash_flow,NetCashfromOperating - PurchasedofFixedAssets + SaleofFixedAssets+capitalWIP+CapitalSubsidyRecd  as FCF from temp4)';
end;
---------------------------------------------------------------------------
create or replace procedure mapping_FF 
is
begin
  execute immediate 'insert into FF (SELECT CO_CODE,CO_NAME,YearEnd,(Cashprofit+Increaseinequity+Increaseinothernetworth+Increaseinloanfunds+Decreaseingrossblock+Decreaseininvestments)-(Cashloss+Decreaseinnetworth+Decreaseinloanfunds+Increaseingrossblock+Increaseininvestments+Dividend) as NetLongTerm,(Decreaseinworkingcapital+Others)-(Increaseinworkingcapital+Others2) as Net_Short_term from temp5)';
end;
----------------------------------------------------------------------------------------------
create or replace procedure mapping_tm 
is
begin
  execute immediate 'insert into technicalsmap(CO_CODE,CO_NAME,year,LatestMarketPriceUnitCurr,LatestPERatio,Week52HighUnitCurr,AllTimeHighUnitCurr,Week52LowUnitCurr,AllTimeLowUnitCurr) (SELECT CO_CODE,CO_NAME,EXTRACT(year FROM pricedate),LatestMarketPriceUnitCurr,LatestPERatio,Week52HighUnitCurr,AllTimeHighUnitCurr,Week52LowUnitCurr,AllTimeLowUnitCurr from latest)';
  execute immediate 'insert into technicalsmap(CO_CODE,CO_NAME,year,enterprisevalue,pricetoearning,pricetocasheps,evtopbidt,dividendyield,pricetobv,pricetocashflow,marketcapsales,bookvalue,eps) (SELECT CO_CODE,CO_NAME,EXTRACT(year FROM dates),EnterpriseValue,PricetoEarning,PricetoCashEPS,EVtoPBIDT,DividendYield,PricetoBV,PricetoCashFlow,MarketCapSales,BookValue,EPS from daily)';
  execute immediate 'insert into technicalsmap(CO_CODE,CO_NAME,year,nse2yearhighprice,nse2yearlowprice,nse3yearhighprice,nse3yearlowprice,nsealltimehighprice,nsealltimelowprice) (SELECT CO_CODE,CO_NAME,EXTRACT(year FROM highlowdate),NSE2YearHighPrice ,NSE2YearLowPrice ,NSE3YearHighPrice ,NSE3YearLowPrice ,NSEAllTimeHighPrice ,NSEAllTimeLowPrice from high)';
end;

---------------------------------------------------------------------------------------------
create or replace procedure mapping_high()
is
begin
  execute immediate 'insert into TM (SELECT CO_CODE,CO_NAME,HIGHLOWDATE,NSE2YearHighPrice ,NSE2YearLowPrice ,NSE3YearHighPrice ,NSE3YearLowPrice ,NSE3YearHighPrice ,NSE3YearLowPrice ,NSEAllTimeHighPrice ,NSEAllTimeLowPrice from high)';
end;
-------------------------------------------------------------------------------------------------
create or replace procedure mapping_daily
is
begin
    execute immediate 'insert into technicalsmap() (SELECT CO_CODE,CO_NAME,Date,EnterpriseValue,PricetoEarning,PricetoCashEPS,EVtoPBIDT,DividendYield,PricetoBV,PricetoCashFlow,MarketCapSales,BookValue,EPS from daily)';
end;
------------------------------------------------------------------------------------------------